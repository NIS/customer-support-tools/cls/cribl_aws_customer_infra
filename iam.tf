data "aws_caller_identity" "current" {}

data "aws_iam_policy_document" "cross-account-assume-role-policy" {
  statement {
    effect = "Allow"

    principals {
      type        = "AWS"
      identifiers = var.principal_arns
    }

    actions = ["sts:AssumeRole"]
  }
}

data "aws_iam_policy_document" "nis-cls-log-access-policy" {
  statement {
    sid    = "S3AccessGet"
    effect = "Allow"

    actions = [
      "s3:GetObject"
    ]

    resources = [
      for bucket in var.s3_buckets : "arn:aws:s3:::${bucket}/*"
    ]
  }

  statement {
    sid    = "S3AccessList"
    effect = "Allow"

    actions = [
      "s3:GetBucketLocation",
      "s3:ListBucket"
    ]

    resources = [
      for bucket in var.s3_buckets : "arn:aws:s3:::${bucket}"
    ]
  }

  statement {
    sid    = "SQSAccess"
    effect = "Allow"

    actions = [
      "sqs:GetQueueAttributes",
      "sqs:GetQueueUrl",
      "sqs:ChangeMessageVisibility",
      "sqs:ReceiveMessage",
      "sqs:DeleteMessage"
    ]

    resources = [
      "arn:aws:sqs:*:${data.aws_caller_identity.current.account_id}:${aws_sqs_queue.nis-cls-cribl-sqs-queue.name}"
    ]
  }
}

resource "aws_iam_role" "cross-account-assume-role" {
  name               = var.role_name
  assume_role_policy = data.aws_iam_policy_document.cross-account-assume-role-policy.json
  tags = merge(var.default_tags, {
    CreatedBy = data.aws_caller_identity.current.user_id,
    CreatedDate = timestamp()
   })

   lifecycle {
    ignore_changes = [tags["CreatedDate"], tags["CreatedBy"]]
  }
}

resource "aws_iam_policy" "nis-cls-log-access-policy" {
  name   = "nis-cls-log-access-policy"
  policy = data.aws_iam_policy_document.nis-cls-log-access-policy.json
  tags = merge(var.default_tags, {
    CreatedBy = data.aws_caller_identity.current.user_id,
    CreatedDate = timestamp()
   })

   lifecycle {
    ignore_changes = [tags["CreatedDate"], tags["CreatedBy"]]
  }
}

resource "aws_iam_role_policy_attachment" "cross-account-access" {
  role       = aws_iam_role.cross-account-assume-role.name
  policy_arn = aws_iam_policy.nis-cls-log-access-policy.arn
}
