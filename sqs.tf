resource "aws_sqs_queue" "nis-cls-cribl-sqs-queue" {
  name                      = var.custom_sqs_queue_name != "" ? var.custom_sqs_queue_name : "nis-cls-cribl-sqs-queue"
  message_retention_seconds = 1209600
  sqs_managed_sse_enabled   = true

  tags = merge(var.default_tags, {
    CreatedBy = data.aws_caller_identity.current.user_id,
    CreatedDate = timestamp()
   })

  lifecycle {
    ignore_changes = [tags["CreatedDate"], tags["CreatedBy"]]
  }
}

#this policy will loop and create it for all S3 buckets provided by the customer
data "aws_iam_policy_document" "nis-cls-cribl-sqs-queue-policy-document" {
  statement {
    effect = "Allow"

    principals {
      type        = "Service"
      identifiers = ["s3.amazonaws.com"]
    }

    actions   = ["sqs:SendMessage"]
    resources = [aws_sqs_queue.nis-cls-cribl-sqs-queue.arn]

    condition {
      test     = "ForAnyValue:StringEquals"
      variable = "aws:SourceArn"
      values   = [for bucket in var.s3_buckets : "arn:aws:s3:::${bucket}"]
    }
  }
}

resource "aws_sqs_queue_policy" "nis-cls-cribl-sqs-queue-policy" {
  queue_url = aws_sqs_queue.nis-cls-cribl-sqs-queue.id
  policy    = data.aws_iam_policy_document.nis-cls-cribl-sqs-queue-policy-document.json
}