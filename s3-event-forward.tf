resource "aws_s3_bucket_notification" "bucket_notification" {
  for_each = toset(var.s3_buckets) 
  bucket   = each.key

  queue {
    queue_arn = aws_sqs_queue.nis-cls-cribl-sqs-queue.arn
    events    = ["s3:ObjectCreated:*"]
  }
}