output "iam_role_arn" {
  value = aws_iam_role.cross-account-assume-role.arn
}

output "sqs_queue_name" {
  value = aws_sqs_queue.nis-cls-cribl-sqs-queue.name
}

output "sqs_queue_url" {
  value = aws_sqs_queue.nis-cls-cribl-sqs-queue.url
}