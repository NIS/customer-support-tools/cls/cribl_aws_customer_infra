variable "aws_region" {
  type    = string
  default = "us-east-1"
}
variable "role_name" {
  type    = string
  default = "nis-cls-log-access"
}

variable "principal_arns" {
  type    = list(any)
  default = ["arn:aws:iam::223431100518:user/nis-cls-cribl"]
}

variable "s3_buckets" {
  type    = list(any)
  default = [""]
}

variable "custom_sqs_queue_name" {
  type    = string
  default = ""
}

variable "default_tags" {
  type = map
  default = {
      CreatedUsing      = "Terraform"
      Purpose           = "CriblLogsExporting"
  }
} 