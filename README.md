# Cribl Stream AWS Infra Documentation
CLS offers [Cribl Stream](https://cribl.io/stream/) service to Virginia Tech customers. Cribl Stream can be used to export logs from AWS S3 back to campus. There are multiple [sources](https://docs.cribl.io/stream/sources/) that Cribl can consume, but this documentation speaks using [Amazon S3 source](https://docs.cribl.io/stream/sources-s3/) and the related infra needed on AWS for Cribl to be able to import logs from Amazon S3. 
## Cribl Infra setup in Customer AWS Account
Per [Cribl Amazon S3 source](https://docs.cribl.io/stream/sources-s3/) documentation, we will need SQS queue that will receive new object created notification from all the S3 buckets that must be exported. Cribl also needs access to poll this SQS queue and also the ability to get objects from S3 buckets. This document provides three ways to provision this infrastructure in your account:
- [Infrastructure Creation using AWS Console](#Infrastructure Creation using AWS Console)
- [Infrastructure Creation using AWS CLI](#Infrastructure Creation using AWS CLI)
- [Infrastructure Creation using Terraform](#Infrastructure Creation using Terraform)
### Infrastructure Creation using AWS Console <a name="Infrastructure Creation using AWS Console"></a>
#### SQS Queue 
SQS queue will receive events from S3 buckets about `s3:ObjectCreated:*` events. Cribl will use these events to identify what objects needs to be pulled from the S3 bucket. To create SQS Queue using AWS console please refer to [AWS documentation](https://docs.aws.amazon.com/AWSSimpleQueueService/latest/SQSDeveloperGuide/creating-sqs-standard-queues.html). Below are the parameters:
- **Name**: `nis-cls-cribl-sqs-queue` (can be replaced with name of your choice)
- **Message retention period**: `14 days` 
- **Access policy**:
  - Select `Advanced` and copy and paste the below policy. Make sure you replace the placeholders with your values. 
    ```json
    {
        "Version": "2012-10-17",
        "Statement": [
            {
                "Effect": "Allow",
                "Principal": {
                    "Service": "s3.amazonaws.com"
                },
                "Action": "sqs:SendMessage",
                "Resource": "arn:aws:sqs:us-east-1:<your-account-number>:<this-sqs-queue-name>",
                "Condition": {
                    "ForAnyValue:StringEquals": {
                    "aws:SourceArn": [
                        "arn:aws:s3:::<bucket-name>",
                        "arn:aws:s3:::<bucket-name-2>"
                    ]
                    }
                }
            }
        ]
    }
    ```
***Note:*** All other fields can be left at default settings.

Please note the `arn` of the SQS queue, this must be provided to CLS during request submission. 
#### Setup Event Notifications on S3 buckets
Create S3 buckets and setup logs exporting from other sources into S3 (customers responsibility), if not already present. 

Once the SQS queue is created, Event Notifications must be setup on all the S3 buckets that must send notifications to SQS queue. To setup notifications please refer to this [AWS documentation](https://docs.aws.amazon.com/AmazonS3/latest/userguide/enable-event-notifications.html)
- **Event name**: `<insert-name>`
- **Event types**: Select `All object create events`
- **Destination**: Select SQS queue and you can either select `choose from your SQS queues` or `Enter SQS queue ARN`. Make sure that the SQS queue chosen here is the queue that was created earlier. 
#### IAM Role 
Please refer to [AWS documentation](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_roles_create.html) on how to setup up IAM role.  
- **IAM Role Name**: `nis-cls-log-access-policy` (cannot be changed)
- **IAM Trust Policy**:
    ```json
    {
        "Version": "2012-10-17",
        "Statement": [
            {
                "Effect": "Allow",
                "Principal": {
                    "AWS": "arn:aws:iam::223431100518:user/nis-cls-cribl"
                },
                "Action": "sts:AssumeRole"
            }
        ]
    }
    ```
- **IAM policy**: 
  - **Name**: `nis-cls-log-access-policy` (can be replaced with name of your choice)
  - **Policy**: Replace placeholder values (`<bucket-1>`, `<aws-region>`, `<account-name>`,`<sqs-queue-name>`) with your values
    ```json
    {
        "Statement": [
            {
                "Action": "s3:GetObject",
                "Effect": "Allow",
                "Resource": [
                    //<insert-all-s3-buckets here>//
                    "arn:aws:s3:::<bucket-1>/*",
                    "arn:aws:s3:::<bucket-2>/*"
                ],
                "Sid": "S3AccessGet"
            },
            {
                "Action": [
                    "s3:ListBucket",
                    "s3:GetBucketLocation"
                ],
                "Effect": "Allow",
                "Resource": [
                    //<insert-all-s3-buckets here>//
                    "arn:aws:s3:::<bucket-1>",
                    "arn:aws:s3:::<bucket-2>"
                ],
                "Sid": "S3AccessList"
            },
            {
                "Action": [
                    "sqs:ReceiveMessage",
                    "sqs:GetQueueUrl",
                    "sqs:GetQueueAttributes",
                    "sqs:DeleteMessage",
                    "sqs:ChangeMessageVisibility"
                ],
                "Effect": "Allow",
                "Resource": [
                    //<insert-all-sqs-queues here, will most likely be only one>//
                    "arn:aws:sqs:<aws-region>:<account-number>:<sqs-queue-name>"
                ],
                "Sid": "SQSAccess"
            }
        ],
        "Version": "2012-10-17"
    }
    ```
    ***Note***: Comments enclosed in `//` must be removed before applying the policy 

Please note the `arn` of the IAM role, this must be provided to CLS during request submission. 

Once all the infrastructure is created, proceed to [Submit a CLS Request](#Submit a CLS Request). 
### Infrastructure Creation using AWS CLI <a name="Infrastructure Creation using AWS CLI"></a>
You can create all the infrastructure through AWS CLI. You must [install AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html). You must also authenticate to your AWS account through CLI. Refer to [AWS CLI Authentication](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-authentication.html)

Once you obtain the credentials and authenticate, you can test your access by running `aws sts get-caller-identity` on the cli. If it doesn't error you can proceed to next steps. 
#### SQS Queue
Download [sqs-queue-attributes.json](reference-policies/sqs-queue-attributes.json) file and update the placeholders with your SQS queue name (one that you are creating) and your s3 buckets information. 
```bash
aws sqs create-queue --queue-name "<sqs-queue-name>" --attributes file://<filepath>/sqs-queue-attributes.json --region <region-name>
```
Upon completion following output will be displayed: 
```json
{
    "QueueUrl": "https://sqs.<region-name>.amazonaws.com/<account-number>/<sqs-queue-name>"
}
``` 
Note the output of this command as this must be provided to CLS.
#### S3 Event Notifications  
Download [notification.json](reference-policies/notifications.json) file and update the placeholders with your specific values.
```bash
aws s3api put-bucket-notification-configuration \
  --bucket <s3-bucket-name> \
  --notification-configuration file://<filepath>/notification.json
```
Repeat the above for all the S3 buckets that must be configured with event notifications. 
#### IAM Role
To create IAM role and attach trust policy download [trust-policy.json](reference-policies/trust-policy.json) file and update the placeholders with your specific values.
```bash
aws iam create-role \
  --role-name nis-cls-log-access \
  --assume-role-policy-document file://<filepath>/trust-policy.json
```
Upon completion following output will be displayed: 
```json
  {
    "Role": {
        ##Redacted##
        "Arn": "arn:aws:iam::<account-number>:role/nis-cls-log-access",
        ##Redacted##
    }
  }
```
To create IAM policies and attach it to the above role download [iam-policy.json](reference-policies/iam-policy.json) file and update the placeholders with your specific values. 
```bash
aws iam put-role-policy \
  --role-name nis-cls-log-access \
  --policy-name nis-cls-log-access-policy \
  --policy-document file://<filepath>/iam-policy.json
```
Once all the infrastructure is created, proceed to [Submit a CLS Request](#Submit a CLS Request). 
### Infrastructure Creation using Terraform <a name="Infrastructure Creation using Terraform"></a>
You can create all the infrastructure needed using terraform. You can run this as standalone deployment using below steps (storing terraform state is not part of these manifests, but please consider storing the terraform state properly so future modifications can be made):
```bash
git clone <git-repo>
cd <folder-name>
export AWS_PROFILE=your-aws-profile
terraform init
terraform plan
terraform apply
```
In standalone deployment below outputs (to be provided to CLS) are show after terraform manifests run successfully:
```bash
iam_role_arn = "arn:aws:iam::<12345678901>:role/nis-cls-log-access"
sqs_queue_name = "nis-cls-cribl-sqs-queue"
sqs_queue_url = "https://sqs.us-east-1.amazonaws.com/12345678901/nis-cls-cribl-sqs-queue"
```
If you are running this as part of the infrastructure deployment, you can call this as a module:
```bash
module "cribl-customer-infra" {
  source = "git::https://code.vt.edu/NIS/customer-support-tools/cls/cribl_aws_customer_infra.git"

  s3_buckets = [<pass the name>] #list
  default_tags = {
    CreatedUsing = "Terraform"
    Purpose      = "CriblLogsExporting"
    #add additional tags as needed below
    <key>        = <value>
  }
}
```
If you are running this as a module, please consider adding outputs from the module, so you can pass them to CLS. 
```bash
output "cls_cribl_iam_role_arn" {
  value = module.cribl-customer-infra.iam_role_arn
}

output "cls_cribl_sqs_queue_name" {
  value = module.cribl-customer-infra.sqs_queue_name
}

output "cls_cribl_sqs_queue_url" {
  value = module.cribl-customer-infra.sqs_queue_url
}
```
Once all the infrastructure is created, proceed to [Submit a CLS Request](#Submit a CLS Request). 

## Submit a CLS Request <a name="Submit a CLS Request"></a>
***<CLS team to add this section, may include how to reach out to CCS, may be a RITM?>***

When submitting a request to CCS, please provide the `SQS-Queue-URL`  and the `IAM-Role-ARN`
```bash
IAM-Role-ARN: "arn:aws:iam::123456789012:role/nis-cls-log-access"
SQS-Queue-URL: "https://sqs.us-east-1.amazonaws.com/123456789012/test-aws-sqs-queue"
```
